# Octabio Motor Helper

This library contains the utility functions:

    void setUpMotor(int motor)

This function is usually called on the setup function on Arduino IDE or PlatformIO, is used to set the necesary pins to the correct mode.
    
    void setMotor(int motor, int speed)

is called to set motor 1 or 2, where motor 1 is the one connected to the D port on the Octabio board and the motor 2 is the one connected to the C port.

## Examples

An example using this library can be found in [Octabio Line Follower](https://gitlab.com/conexalab/octabio_line_follower)
